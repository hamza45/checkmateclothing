﻿using CheckMateClothing.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckMateClothing.Database
{
  public  class CMCContext : DbContext
  {


      public CMCContext() : base("CheckMateClothingConnection")
      {




      }



        public DbSet<Products> Products { get; set; }

        public DbSet<Category> Categories { get; set; }


    }
}
