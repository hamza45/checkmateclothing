﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CheckMateClothing.Web.Startup))]
namespace CheckMateClothing.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
